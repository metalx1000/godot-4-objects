extends CharacterBody2D


const SPEED = 200.0
const JUMP_VELOCITY = -400.0
var score = 0
var state = "idle"

@onready var animation = $AnimationPlayer
@onready var sprite = $Sprite2D
@onready var groundcast_l = $groundcast_l
@onready var groundcast_r = $groundcast_r

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")


func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y += gravity * delta

	# Handle Jump.
	if Input.is_action_just_pressed("player_jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var direction = Input.get_axis("player_left", "player_right")
	if direction:
		velocity.x = direction * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
	
	
	move_and_slide()
	set_state()

func attack(obj):
	if velocity.y< 1:
		return
		
	if obj.has_method("take_damage"):
		obj.take_damage(1)
		velocity.y = JUMP_VELOCITY / 2


func set_state():
	if velocity.x != 0 && is_on_floor():
		if velocity.x > 0:
			sprite.flip_h = false
		elif velocity.x < 0:
			sprite.flip_h = true
		state = "walk"
	else:
		state = "idle"
		
	animation.play(state)
	

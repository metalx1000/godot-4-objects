extends RigidBody2D


const SPEED = 300.0
const JUMP_VELOCITY = -400.0

@export var health : int = 1
@onready var animation = $AnimationPlayer
@onready var collision = $CollisionShape2D
# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

func _ready():
	animation.play("RESET")

func _physics_process(delta):
	# Add the gravity.
	#if not is_on_floor():
	#	velocity.y += gravity * delta

	#move_and_slide()
	pass

func take_damage(amount):
	health -= 1
	if health < 1:
		death()

func death():

	animation.play("death")

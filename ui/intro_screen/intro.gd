extends Control

@onready var player = $AnimationPlayer
@onready var label = $Label
var msg_num = 0

@export_file var next_scene

var texts = [
	"FBK GAMES",
	"©Kris Occhipinti\n2024",
	"My Game
Copyright (C) 2023  Kris Occhipinti
https://filmsbykris.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>."
]

# Called when the node enters the scene tree for the first time.
func _ready():
	label.text = texts[msg_num]

func _process(_delta):
	if Input.is_action_just_pressed("ui_accept"):
		next_msg()
	
func next_msg():
	player.seek(0)
	msg_num = msg_num + 1
	if msg_num >= texts.size():
		get_tree().change_scene_to_file(next_scene)
	else:
		label.text = texts[msg_num]
